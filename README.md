# SMRT® Link demuxer
Takes CCS reads after PacBio SMRT® Link "barcoding" (mapping via MID tags) step and aggregates them into human-readable format for futher analysis. 
If reference sequences are available (i.e. gold standard), can estimate accuracy of the barcoding. 

**Input:** multiple ccsXXX_XXX.fastq

**Ouput:** tab-delimited.txt


## Usage
```<smrt_out_processor.py> [parameters]```

#### parameters:
```
--meta-data  - path to Excel file containing sample, process and plate ID information (*)

--mids-file-seqs  - FASTA file with mid-tags/barcodes with matched indexing

--ref-seq  (optional) - FASTA file with reference sequences (if available)

--in-fastq-dir - directory with input fastq files names ccsXXX_XXX.fastq where XXX is MID-tag index

--output - output path for tab-delimited txt file
```
\* - a sample meta data file contents and the key headers

|Forward|Reverse|ProcessID|SampleID|384_Plate ID|Coordinate	|
|-------|-------|---------|--------|------------|-----------|
|0010016_AsF_lbc274|0023_AsR_lbc62|PACBC1521-16|BIOUG31216-A01.PacBio|PBPCR2-55|A01|
|0001_AsF_lbc13|0009_AsR_lbc240|PACBC1525-16|	BIOUG31216-A05.PacBio|PBPCR2-55	|A09|