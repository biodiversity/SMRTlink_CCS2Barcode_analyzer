#!/usr/bin/env python
# input - SMRTLink PacBio processed input after bacoding/mid-tag mapping step
# output - Trimmed reads with estimated error-rates to the reference sequence if supplied
# #input from meta excel file (containing ProcessID, SampleID)
    #repertoir of the mid-tags


import sys
import linecache   #to read specific line of the file
import argparse
from openpyxl import load_workbook   # pip install openpyxl
import glob
import csv
import re #exact string matching
import tre #fuzzy regex matching
#from subprocess import call
import collections
from Bio.Seq import Seq
from Bio import SeqIO
#import copy
import time
import Levenshtein
#from Bio import pairwise2



class ReadMetaInput():

    def read_excel(self, f):
        wb = load_workbook(f, read_only=True);
        ws = wb.active;

        title_line = [ws.cell(row=1, column=c).value for c in range(1,20)];
        search_headers = ["384_Plate ID","ProcessID", "SampleID","Coordinate", "Forward", "Reverse"]
        c_idx =  [ title_line.index(name) for name in  search_headers] #extract the columns
        idx_dict = dict(zip (search_headers, c_idx))

        raw_data = [d for d in ws["A{}:Z{}".format(2, ws.max_row)]]  # skip the 1st line design as header

        meta_data={}
        for r in range(0, len(raw_data)):
            plateid = raw_data[r][idx_dict["384_Plate ID"]].value  #processID
            pid = raw_data[r][idx_dict["ProcessID"]].value  #processID
            sid = (raw_data[r][idx_dict["SampleID"]].value); #sampleID
            if sid:
                sid=sid.rstrip(".PacBio");
            else:
                sid=""
            if pid is not None:
                meta_data[r]= {"pid": pid, \
                               "sid": sid, \
                               "plateid": plateid,\
                               "crd": raw_data[r][idx_dict["Coordinate"]].value, \
                               "mid_f": raw_data[r][idx_dict["Forward"]].value, \
                               "mid_r": raw_data[r][idx_dict["Reverse"]].value, \
                               "bcodes": [],
                               "file_source": []
                               }
        return meta_data

class MultiFastqReads():
    n_in_fastqs=0
    in_fastqs = []
    max_files2process = None

    def __init__(self, path, dataframe, mids_idx_map_dict, max_n_files=None):
        self.in_fastqs = glob.glob(path+"/*.fastq")
        self.n_in_fastqs = len(self.in_fastqs);
        self.max_files2process=max_n_files;
        print "Total fastq files to process: " + str(self.n_in_fastqs)

        if len(self.in_fastqs) == 0:
            sys.exit("\n\nERROR: No FASTQ files found in "+path+" directory \nAborting ...\n")

        counter=0
        for file_path in self.in_fastqs:
            print "Path "+file_path
            reads = self.read_fastq(file_path)
            print("Total reads in a fastq file: "+str(len(reads)))


            if reads and all([l == "A" or l == "T" or l == "G" or l == "C" for r in reads for l in r]):
                lookup_idx = self.mapreads2well(file_path, dataframe, mids_idx_map_dict);
            else:
                sys.exit("\nERROR: Input sequences are not DNA!\nERROR: Check contents of "+file_path+"\n")

            if lookup_idx:
                dataframe[lookup_idx]["bcodes"] = reads;
                m=re.compile('ccs\.{1,1}.*fastq'); file = m.search(file_path).group(0)
                dataframe[lookup_idx]["file_source"].append(file);
                #print file; sys.exit()
            else:
                print "Failed to map "+ file_path

            #print dataframe[lookup_idx]
            counter += 1

            if self.max_files2process and counter > self.max_files2process:
                print "INFO: Read "+str(counter)+" fastq files (i.e. read limit is set)"
                break
            #    print("Done with fastq "+file_path)

    def read_fastq(self, file):
        reads=[]
        nseq_lines = self.get_seq_lines_count(file)
        for n_line in nseq_lines:
            line = linecache.getline(file, n_line).rstrip("\n")
            reads.append(line.rstrip("-"))
        return reads

    def get_seq_lines_count(self, file):
        with open(file) as f:
            total_lines = sum(1 for _ in f)
        return range(2,total_lines, 4)

    def mapreads2well(self, file_path,metadata, mids_dict):
        m=re.compile('(ccs.)(\d+_\d+)(.fastq)'); indices = m.search(file_path).group(2).split("_");
        indices = [int(i) for i in indices]

        mid_id_fwd_file = mids_dict[indices[0]].keys()[0]; mid_id_rev_file = mids_dict[indices[1]].keys()[0];
        m = re.compile('(F_\d+_+)(\d+_.*)'); key_fwd = m.search(mid_id_fwd_file).group(2);
        m = re.compile('(R_\d+_+)(\d+_.*)'); key_rev = m.search(mid_id_rev_file).group(2);

        idx = [i for i in range(0,len(metadata)) if(metadata[i]["mid_f"] == key_fwd and metadata[i]["mid_r"] == key_rev)]

        if idx and len(idx) == 1:
            return idx[0]

        #print sorted(set([metadata[k]["mid_r"] for k in metadata]))



class Dist2ReferenceSeqs():

    def __init__(self):
        self.ref_seq_dict={}

    def read_fasta(self, in_f):
        with open(in_f, "rU") as fp:
            for record in SeqIO.parse(fp, "fasta"):
                self.ref_seq_dict[record.id] = str((record.seq).rstrip("-"))


class Utilities():
    def fastafile2dict(self, in_f):
        data_ordered_dict={}
        with open(in_f, "rU") as fp:
            counter = 0;
            fasta_obj = SeqIO.parse(fp, "fasta");

            for record in fasta_obj:
                data_ordered_dict[counter]={record.id: str((record.seq).rstrip("-"))}
                counter += 1
        print "MID Records "+ str(counter);
        return(data_ordered_dict)


class WriteResultsDataOut():
    def __init__(self, f_out):
        with open(f_out, "w")  as fp:
            fp.close()

    def overlap_dist2ref(self, bc, meta_rec, ref_seq_dict):
        #print (hex(id(bc))); sys.exit()
        sid = meta_rec["sid"]

        k_ref = [k for k in ref_seq_dict.keys() if re.search(sid, k)]
        if k_ref:
            k_ref = k_ref[0]
        else:
            return "NoRef", "NoRef", "NoRef", "NoRef", "NoRef", "NoRef", bc
        ref_seq = ref_seq_dict[k_ref]

        tp = tre.compile(ref_seq)
        fz = tre.Fuzzyness(maxerr=50)
        m = tp.search(bc, fz);


        if m:
            start = m.groups()[0][0]; end = m.groups()[0][1]; consensus_bc = bc[start:end];
        else:
            m = tp.search(str(Seq(bc).reverse_complement()), fz);
            if m:
                bc = str(Seq(bc).reverse_complement())
                start = m.groups()[0][0]; end = m.groups()[0][1]; consensus_bc = bc[start:end];
            else:
                return ">50", "", "", "", "", k_ref, bc

        tp = tre.compile(consensus_bc)
        m = tp.search(ref_seq, fz);
        consenus_ref_seq="";

        if m:
            start = m.groups()[0][0];
            end = m.groups()[0][1]
            consenus_ref_seq = ref_seq[start:end]

        # calculate overlap distance
        max_olp_len = max(len(consensus_bc), len(consenus_ref_seq))
        dist_obj = Levenshtein.editops(consensus_bc, consenus_ref_seq)
        if len(dist_obj) == 0:  # perfect match
            return 0, 0, 0, max_olp_len, 0, k_ref, bc

        # breakdown distance to indels and subst counts
        dist = len(dist_obj)
        counter = collections.Counter(zip(*dist_obj)[0])
        editop_dict = dict(counter.most_common())
        indels = 0;
        subs = 0
        for k in editop_dict:
            if k == "delete" or k == "insert":
                indels = indels + editop_dict[k];
            if k == "replace":
                subs = subs + editop_dict[k]

        error_rate = (float(dist) / float(max_olp_len)) * 100
        return dist, indels, subs, max_olp_len, error_rate, k_ref, bc



    def write_results_out(self, df, f_out, ref_seq_obj):
        print "\nWritting results output ..."

        with open(f_out,"a") as fp:
            fp.write('{0!s}\t{1!s}\t{2!s}\t{3!s}\t{4!s}\t{5!s}\t{6!s}\t{7!s}\t{8!s}\t{9!s}\t{10!s}\t{11!s}\t{12!s}\t{13!s}\n'.format("ProcessID","SampleID","PlateID","Coordinate", "DescriptiveID", "Barcode",
                                                                 "Nreads","OvlpDist2Ref", "Indels", "Subst", "MaxOverlap(bp)", "%Error2Ref", "RefSeqID", "FileSource"))
            counter=0
            for key in df.keys():  # row number of the meta data {0..n}
                record = df[key]   # e.g. recod = {'mid_r': {u'0001_AsR_lbc76_rc': 'ACGTGAGCTCACTCGC'}, 'pid': u'PACBC001-16', 'bcodes': [], {u'0001_AsF_lbc13': 'ACACTGACGTCGCGAC'} ... }


                bcs, fq = self.find_unq_bc(record) #return unique barcodes and their frequencies

                if bcs:
                    for n in range(0,len(bcs)): #loop through all barcodes assigned to a well
                            if len(bcs[n]) > 0: #in some cases some records of the original data frame do not have any bcs due to failed MID-tag mapping
                                bc=bcs[n]
                                if ref_seq_obj.ref_seq_dict:
                                    d2r, n_indels, n_subs, cons_len, error, ref_id, bc = self.overlap_dist2ref(bc,record, ref_seq_obj.ref_seq_dict)
                                else: #if no refrence is supplied
                                    d2r = " "; n_indels=" "; n_subs=" "; error=""; cons_len = " "; ref_id = " ";

                                #print record["mid_f"].keys()
                                fp.write('{0!s}\t{1!s}\t{2!s}\t{3!s}\t{4!s}\t{5!s}\t{6!s}\t{7!s}\t{8!s}\t{9!s}\t{10!s}\t{11!s}\t{12!s}\t{13!s}\n'.format(record["pid"],
                                    record["sid"],record["plateid"],record["crd"],
                                    str(record["pid"]+"["+record["mid_f"]+"+"+record["mid_r"]+"]_"+str(n+1)), bc, fq[n], d2r, n_indels, n_subs, cons_len, error, ref_id,record["file_source"]))
                counter = counter + 1
            fp.close()

    def find_unq_bc(self, recd):
        counter = collections.Counter(recd["bcodes"])
        mc = counter.most_common()
        barcodes = [];
        bc_freq = []
        if mc:
            n = len(mc)
            barcodes[0:n] = [mc[i][0] for i in range(0, n)]
            bc_freq[0:n] = [mc[i][1] for i in range(0, n)]
        return barcodes, bc_freq




if __name__ == '__main__':
    print "-"*40
    print "SMRTLink output post-processor 1.0"
    print "-"*40

    parser = argparse.ArgumentParser(
        description='Reads PacBio SMRTLink raw mapped CCS reads and concatinates meta data and quality metrics (if reference sequences are available)');
    parser.add_argument('-i', '--in-fastq-dir', nargs=1, dest="in_fastq_dir", default=None, metavar='', type=str, help = 'Input path to FASTQ files directory');
    parser.add_argument('-mids_file', '--mids-file-seqs', nargs=1, dest="mids_file", default=None, metavar='', type=str, help='Input path to paired MID-tag sequences in FASTA format');
    parser.add_argument('-m', '--meta-data', nargs=1, dest="meta_file", default=None, metavar='', type=str, help='Input path to META data file');
    parser.add_argument('-rs', '--ref-seq', dest="ref_seq", nargs=1, default="", metavar='', help='Input file with the reference barcode sequences');
    parser.add_argument('-o', '--output', dest="output", nargs=1, default=None, metavar='', help='Output results file (e.g. output.txt)');

    args = parser.parse_args();

    if any(vars(args)[k] == 0  for k in vars(args).keys() ) == True:
        parser.print_help(); sys.exit("\nERROR: Not all parameters supplied. Check and try again!");

    meta = ReadMetaInput();
    df = meta.read_excel(args.meta_file[0]);

    ut = Utilities(); mids_dict = ut.fastafile2dict(args.mids_file[0]);
    MultiFastqReads(args.in_fastq_dir[0], df, mids_dict);  # fastq object with all the reads

    refseqs = Dist2ReferenceSeqs();
    refseqs.read_fasta(args.ref_seq[0]) #get the ref_seq dictionary

    w = WriteResultsDataOut(args.output[0])
    w.write_results_out(df, args.output[0], refseqs)













